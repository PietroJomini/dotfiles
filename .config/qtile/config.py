from keys import keys
from groups import groups, gkeys
from layouts import layouts
from screens import screens
from widgets import widget_defaults
from floating import mouse, floating_layout
from options import *

keys.extend(gkeys)
