from libqtile import layout
from colors import palette

from mmax import MMax

defaults = dict (
    border_width = 2,
    margin = 10,
    border_focus = palette['colors']['color2'],
    border_normal = palette['special']['background']
)

layouts = [
    layout.MonadTall(
        border_width = 2,
        margin = 10,
        border_focus = palette['colors']['color2'],
        border_normal = palette['special']['background'],
        single_border_width = 0
    ),
    MMax(
        margin = 10
    )
]