from libqtile.config import Key
from libqtile.command import lazy
from libqtile import extension

from options import mod

keys = [

    # Core management
    Key([mod, 'control'], 'r', lazy.restart()),
    Key([mod, 'control'], 'q', lazy.shutdown()),

    # Sound
	Key([], 'XF86AudioMute', lazy.spawn('amixer -q set Master toggle')),
	Key([], 'XF86AudioLowerVolume', lazy.spawn('amixer -c 0 sset Master 1- unmute')),
	Key([], 'XF86AudioRaiseVolume', lazy.spawn('amixer -c 0 sset Master 1+ unmute')),
	Key(['shift'], 'XF86AudioLowerVolume', lazy.spawn('amixer -c 0 sset Master 9- unmute')),
	Key(['shift'], 'XF86AudioRaiseVolume', lazy.spawn('amixer -c 0 sset Master 9+ unmute')),

    # Winwods management
	Key([mod], 'space', lazy.spawn('dmenu_run -p \'Run: \'')),
	Key([mod], 'c', lazy.window.kill()),
    Key([mod], 'Return', lazy.spawn('alacritty')),

    # Toggle between layouts
    Key([mod], 'Tab', lazy.next_layout()),

    # MonadTall layout
    Key([mod], 'h', lazy.layout.left()),
    Key([mod], 'j', lazy.layout.up()),
    Key([mod], 'k', lazy.layout.down()),
    Key([mod], 'l', lazy.layout.right()),
    Key([mod, 'shift'], 'h', lazy.layout.swap_left()),
    Key([mod, 'shift'], 'j', lazy.layout.shuffle_down()),
    Key([mod, 'shift'], 'k', lazy.layout.shuffle_up()),
    Key([mod, 'shift'], 'l', lazy.layout.swap_right()),
    Key([mod], 'i', lazy.layout.grow()),
    Key([mod], 'o', lazy.layout.shrink()),
    Key([mod], 'm', lazy.layout.maximize()),
    Key([mod], 'n', lazy.layout.normalize()),
    Key([mod, 'shift'], 'space', lazy.layout.flip()),

    # Test
    Key([], 'XF86Launch1', lazy.spawn('alacritty')),

]
