from libqtile.config import Group, Key
from libqtile.command import lazy

from options import mod

groups = [Group(i) for i in '123456789']

gkeys = []
for group in groups:
    gkeys.extend([
        Key([mod], group.name, lazy.group[group.name].toscreen()),
        Key([mod, 'shift'], group.name, lazy.window.togroup(group.name)),
    ])