from libqtile import widget

from colors import palette

special = palette['special']
colors = palette['colors']

widget_defaults = dict (
    fonts = 'sans',
    fontsize = 12,
    padding = 3
)

spacer = widget.Spacer()

groupbox = widget.GroupBox (
    active = special['foreground'],
    block_highlight_text_color = special['background'],
    highlight_method = 'line',
    highlight_color = [special['foreground'], special['foreground']],
    borderwidth = 2,
    this_current_screen_border = special['foreground'],
    inactive = special['background'],
    hide_unused = True,
    margin = 5,
    rounded = False
)

volume = widget.Volume(
    foreground = special['foreground'],
    step = 1,
    emoji = False,
    fmt = ' vol [ {} ] '
)

battery = widget.Battery(
    format = ' bat [ {char}{percent:2.0%} ] ',
    full_char = '',
    update_interval = 1,
    foreground = special['foreground'],
    discharge_char = '',
    charge_char = '^',
    low_foreground = colors['color1'],
    low_percentage = 0.2,
    show_short_text = False
)

clock = widget.Clock (
    format = ' [ %d/%m %H:%M ]',
    foreground = special['foreground']
)

wlan = widget.Wlan (
    foreground = special['foreground'],
    interface = 'wlo1',
    format = ' wlo1 [ {essid} {percent:2.0%} ] ',
    disconnected_message = ' wlo1 disconnected '
)