from libqtile.config import Drag, Click
from libqtile.command import lazy
from libqtile import layout

from options import mod

mouse = [
    Drag([mod], 'Button1', lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], 'Button3', lazy.window.set_size_floating(), 
        start=lazy.window.get_size()),
    Click([mod], 'Button2', lazy.window.bring_to_front())
]

floating_layout = layout.Floating ( float_rules = [
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},    # gitk
    {'wmclass': 'makebranch'},      # gitk
    {'wmclass': 'maketag'},         # gitk
    {'wname':   'branchdialog'},    # gitk
    {'wname':   'pinentry'},        # GPG key password entry
    {'wmclass': 'ssh-askpass'},     # ssh-askpass
])