from libqtile.config import Screen
from libqtile import bar

from widgets import spacer, groupbox, volume, battery, clock, wlan
from colors import palette

topbar = bar.Bar([
        groupbox,
        spacer,
        wlan,
        volume,
        battery,
        clock
    ], 30,
    margin = [10, 10, 0, 10],
    background = palette['special']['background']
)

screens = [
    Screen(
        top = topbar
    )
]
