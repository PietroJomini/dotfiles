from libqtile.layout.base import _SimpleLayoutBase

class MMax(_SimpleLayoutBase):
    """Margin + Maxlayout
    I couldn't find a way to put margin on Max layout
    """
    
    defaults = [
        ('name', 'mmax', 'Name of this layout'),
        ("margin", None, "Margin size for single window"),
    ]

    def __init__(self, **config):
        _SimpleLayoutBase.__init__(self, **config)
        self.add_defaults(MMax.defaults)

    def clone(self, group):
        return _SimpleLayoutBase.clone(self, group)

    def add(self, client):
        return self.clients.add(client, 1)

    def configure(self, client, screen):
        if self.clients and client is self.clients.current_client:
            client.place(
                screen.x,
                screen.y,
                screen.width,
                screen.height,
                0,
                None,
                margin=self.margin
            )
            client.unhide()
        else:
            client.hide()
        
    cmd_previous = _SimpleLayoutBase.previous
    cmd_next = _SimpleLayoutBase.next
    
    cmd_up = cmd_previous
    cmd_down = cmd_next